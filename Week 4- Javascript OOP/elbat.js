export default class QuickTable{
    constructor(init){
        this.init = init
        
    }

    header(repr){
        let open = "<thead><tr>"
        let close ="</tr></thead>"
        repr.forEach((d) => {
            open += `<th>${d}</th>`
        });
        
        return open +close
    }
    body(repr){
        let open ='<tbody>'
        let close ='</tbody>'
        repr.forEach((d)=> {
            open+='<tr>'
            d.forEach((p)=>{
                open+= `<td>${p}</td>` 
            })
            open+='</tr>'
        })
        return open+close
    }
    show(target){
        let scr = "<table class='table table-hover'>"+this.header(this.init.columns)+this.body(this.init.data)+"</table>";
        target.innerHTML =scr
    }
    
}

