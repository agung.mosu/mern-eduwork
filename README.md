# Mern Eduwork
# Week 1 - Git

## Git Init
Digunakan untuk initialisasi git pada folder
syntax :
```
git init
```
maka akan muncul
```
Initialized empty Git repository in D:/WorkSpace/MernEduwork/.git/
```
## Git Status
Digunakan untuk menampilkan status dari working tree dalam folder

```
git status
```

jika tidak ada yang terjadi hasilnya adalah

```
nothing to commit, working tree clean
```

jika ada perubahan maka hasilnya adalah

```
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

## Git add
git add merubah status file dari untracked/modified kemode staged, yaitu mode sebelum commit
syntax :

```
git add .
git add <nama File.extension>
```

jika sudah maka menghasilkan

```
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
```

## Git Commit
digunakan untuk commit perubahan pada git

```
git commit
```

jika berhasil akan menampilkan perubahan

```
1 file changed, 16 insertions(+), 2 deletions(-)
```

## Git Remote
Semacam API untuk bisa saling berkolaborasi 
untuk menambah remote kita bisa menggunakan

```
git remote add <nama remote> <link>
```

kita bisa melakukan check remote apa saja yang ada dengan 

```
git remote -v
```

## Git Push & git Pull
digunakan untuk melakukan request ke git Cloud
syntax :

```
git push <nama remote> // untuk mengupload
git pull <nama remote> // untuk mendownload
```

## Git Branch
digunakan untuk membuat cabang baru dari cabang utama untuk memudahkan dalam mendevelop fungsi baru

syntax :

```
git branch <nama branch>
```

untuk menggunakan branch tersebut kita menggunakan checkout

```
git checkout <nama branch>
```

## Git merge

digunakan untuk melakukan merge branch yang mengacu pada branch aktif

syntax :

```
git merge <nama branch>
```