/* <input type="hidden" name="apiKey" value="5610b3837a5d4ae4b50a00a3319a425e">
                <input type="hidden" name="pageSize" value="8"></input> */


let lnNews =""
let lnNewsId =""
document.addEventListener("DOMContentLoaded",async function(){
    console.log("DOM has been fully loaded and parsed.");
    let latestNews = await fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=5610b3837a5d4ae4b50a00a3319a425e&pageSize=3`)
    let lnReady = await latestNews.json()
    console.log(lnReady);
    lnReady.articles.forEach(data=>{
                
        
        lnNews+=
        `
        <div class="col"><a href="${data.url}">
            <div class="card my-2" style="width: 16rem;">
              <img id="imgNews" src="${data.urlToImage}" class="card-img-top">
                  <div class="card-body">
                    <h6 class="card-title">${data.title}</h6>
                    <h6 class="card-title text-muted">${data.author}</h6>
                    <p class="card-subtitle text-muted"> ${data.publishedAt}</p>
                </div>
            </div></a>
        </div>
        `
    })
    document.getElementById('latestNews').innerHTML= lnNews
    let latestNewsId = await fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=5610b3837a5d4ae4b50a00a3319a425e&pageSize=8`)
    let lnReadyId = await latestNewsId.json()
    // console.log(lnReady);
    lnReadyId.articles.forEach(data=>{
                
        
        lnNewsId+=
        `
        <div class="col"><a href="${data.url}">
            <div class="card my-2" style="width: 12rem;">
              <img id="imgNews" src="${data.urlToImage}" class="card-img-top">
                  <div class="card-body">
                    <h6 class="card-title">${data.title}</h6>
                    <h6 class="card-title text-muted">${data.author}</h6>
                    <p class="card-subtitle text-muted"> ${data.publishedAt}</p>
                </div>
            </div></a>
        </div>
        `
    })
    document.getElementById('newsTemplate').innerHTML= lnNewsId
});

let timer=null;

document.getElementById('loading').style="display:none"
function fetchData(){
    
    document.getElementById('loading').style="display:block"
    let resultNews = "";
    
    
    if (search.value=="") {
        clearTimeout(timer)
        document.getElementById('loading').style="display:none"
    }else{
        document.getElementById('newsTemplate').innerHTML = ""
        clearTimeout(timer)
        timer = setTimeout(async ()=>{
            console.log(search.value)
            let package = await fetch(`https://newsapi.org/v2/everything?q=${search.value}&apiKey=5610b3837a5d4ae4b50a00a3319a425e&pageSize=8`)
            let dataReady = await package.json()
            dataReady.articles.forEach(data=>{
                
                document.getElementById('loading').style="display:none"
                resultNews+=
                `
                <div class="col"><a href="${data.url}">
                    <div class="card my-2" style="width: 12rem;">
                      <img id="imgNews" src="${data.urlToImage}" class="card-img-top">
                          <div class="card-body">
                            <h6 class="card-title">${data.title}</h6>
                            <h6 class="card-title text-muted">${data.author}</h6>
                            <p class="card-subtitle text-muted"> ${data.publishedAt}</p>
                        </div>
                    </div></a>
                </div>
                `
            }) 
         
            document.getElementById('newsTemplate').innerHTML = resultNews
        },2000)
        
        
    }

}